Lacuna PKI SDK changelog
========================

1.12.1 (2016-06-03)
-------------------

- Add support for decoding of ICP-Brasil certificate fields "OAB" and "RG"
- Add support on attribute certificate generation for the AuthorityKeyIdentifier extension
- Introduce new class CadesAcceptablePoliciesCatalog to simplify validation of CAdES signatures accepting multiple signature policies
- Introduce new class Rfc3161Encoding allowing applications to implement the timestamp protocol (TSP)
- Introduce specialized exception class TimestampRequestException for identifying errors when requesting timestamps
- Add support for certificates with rare alternative SHA-1 with RSA signature algorithm OID (1.3.14.3.2.29)
- Introduce new class FileSystemNonceStore with a dependency-free implementation of the INonceStore interface


1.12.0 (2016-05-02)
-------------------

- Introduce new class PdfMarker for performing PDF marks
- Introduce new interface ITimestampRequester allowing customization of timestamping
- Add support for customization of culture, format and time zone of the signing time in PAdES visual representation
- Add support for performing CAdES signatures (without encapsulated content) passing message digest only
- Add support for adding encapsulated content to CAdES signature
- Add ICP-Brasil trusted root "v5"
- Fix issue affecting validation of XML signatures having namespace declarations on the Signature element
- Fix issue affecting positioning of PAdES visual representations in specific several-signers scenarios
- Fix issue causing unnecessary PIN dialogs when listing certificates


1.11.2 (2016-03-27)
-------------------

- Fix bug on CRL decoding when the ReasonCode is present
- Improve messages for certificate revocation status validation


1.11.1 (2016-03-24)
-------------------

- Fix bug on certificate revocation status validation which caused a stack overflow on rare OCSP validation scenarios


1.11.0 (2016-02-22)
-------------------

- SDK no longer requires the iTextSharp AGPL-licensed library in order to perform PAdES signatures
- Deprecated package Lacuna.Pki.PadesConnector, since PAdES signatures are now performed internally by the SDK


1.10.1 (2016-02-15)
-------------------

- Fix bug on PAdES signature which caused an incompatibility with certain TSAs which issued timestamps larger than
  expected
- Fix bug on attribute certificate generation which prevented the AuthorityInformationAccess extension from being added
  when the CAIssuersUri was given
- Fix bug on attribute certificate validation concerning validation of the "IsCA" basic constraint on the issuer
  certificate
- Fix bug on attribute certificate validation when the NoRevocationAvailable extension is present


1.10.0 (2016-02-13)
-------------------

- Add support for user-extensible private keys
- Add support on Asn1Util class for encoding the ASN.1 type "sequence of"
- Add support for decoding the "DNS Names" certificate extension
- Add support for clearing the previously loaded SDK license


1.9.1 (2016-02-11)
------------------

- Fix bug on encoding of ASN.1 structure AlgorithmIdentifier which caused the field "parameters" to be omitted instead
  of being filled with the NULL value


1.9.0 (2016-01-21)
------------------

- Add support for XML signatures (XmlDSig/XAdES)
- Add support for propagating changes to a signature policy's trust arbitrators to its timestamp validation policies
- Improve certificate validation to check the PathLenConstraint extension
- Improve CAdES signature validation on cases on which the signer certificate could not be found
- Fix bug on validation of licenses without the "restricted use" field
- Fix bug on CAdES signature validation which caused a wrong ValidationItem to be informed when no valid signature
  timestamp was found (was informing InvalidReferencesTimestamp, correct is InvalidSignatureTimestamp)


1.8.0 (2015-11-23)
------------------

- Add support on PAdES visual representation for specifying a container inside the signature rectangle on which to
  place the text


1.7.2 (2015-11-12)
------------------

- Modify behavior of property IcpBrasilCertificateFields.CompanyName to return the company name when the certificate is
  a ICP-Brasil company (PJ) certificate (previously the property only worked for ICP-Brasil application certificates)
- Fix bug on ICP-Brasil AD-RC v2.1 CAdES signature policy (policy OID was incorrect)
- Fix bug on validation of CAdES signatures using implict policies


1.7.1 (2015-10-30)
------------------

- Add support for ICP-Brasil CPF field on "OU" field of subject name having a space after the colon ("OU=CPF: xxxxxxxxxxx")


1.7.0 (2015-10-27)
------------------

- Add support for licenses with use restricted to certain Lacuna products
- Modify behavior of IcpBrasilCertificateFields class to try to decode fields regardless of whether the certificate
  appears to be an ICP-Brasil certificate or not
	- *Notice: the correct way to verify if a certificate is a valid ICP-Brasil certificate is to validate it using the
	  TrustArbitrators.PkiBrazil trust arbitrator, not through its fields*


1.6.0 (2015-10-14)
------------------

- Add support on PAdES visual representation for horizontal text alignment to the right
- Introduce class PkiInfo with information about the loaded SDK license
- Fix bug on logging which caused the "source" argument to have an incorrect value


1.5.8 (2015-09-30)
------------------

- Add check of signature value on PadesSigner
- Fix bug that caused classes WindowsCertificateStore and Pkcs12CertificateStore to return an empty list of
  certificates instead of throwing an exception when the SDK license was not found
- Fix bug in the backward compatibility for the old PAdES visual representation


1.5.7 (2015-09-28)
------------------

- Refactor of PAdES visual representation (introducing new class PadesVisualRepresentation2)
- Fix bug on Italy CAdES policy which caused additional certificates and CRLs to be wrongly added to the SignedData


1.5.6 (2015-08-31)
------------------

- Add support for supplying an external certificate store to the PadesSigner class
- Fix bug on certificate revocation status check when an external certificate store was required
- Fix bug on properties DateOfBirth and CertificateType of class IcpBrasilCertificateFields which caused an exception
  to be thrown if the certificate was badly issued/encoded (behavior now is to return null)


1.5.5 (2015-08-14)
------------------

- Modify licensing to accept license files with .config extension only to prevent leaking of the license files
	- *Notice: licenses issued prior to the date of this release are still accepted with a .xml extension*


1.5.4 (2015-07-30)
------------------

- Improve certificate revocation status check
- Improve validation of PAdES signatures to consider internal signature timestamps


1.5.3 (2015-07-30)
------------------

(version not released)


1.5.2 (2015-07-23)
------------------

- Add support for password-based authentication with timestamp servers
- Improve validation messages for errors on revocation status check
- Improve intellisense documentation
- Deprecated property PkiConfig.MaxDelay in favor of property PkiConfig.PreemptiveDownloadMaxDelay


1.5.1 (2015-07-16)
------------------

- First publicly available version
